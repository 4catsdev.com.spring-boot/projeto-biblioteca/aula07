package com.fourcatsdev.aula07.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula07.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
